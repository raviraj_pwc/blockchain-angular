import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {testDataStructure} from "./testDataStructure";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import { TestApiService} from "../test-api.service";

@Component({
  selector: 'app-test-app',
  templateUrl: './test-app.component.html',
  styleUrls: ['./test-app.component.scss']
})

export class TestAppComponent {

  username: string="";
  test=new testDataStructure;
  characters: PeriodicElement;
  types: PeriodicElement[]=[{}];
  constructor(private http:HttpClient, private tservice: TestApiService) {

    this.tservice.getParticipants().subscribe((data) => {
      this.types[0]=this.characters;
      console.log("zero elemetn", this.types)
    });
  }
  onSubmit(){

    alert("Thank you for your submission! Data: "+ JSON.stringify(this.test));
    console.log(this.test);
    console.log(JSON.stringify(this.test))

    this.http.post('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/participant',JSON.stringify(this.test))
      .subscribe(
        ((data)=>{
          console.log(data)
          alert("Your Participant Key is "+ data +". \nSave it for future reference.");

          }
        ),
        err => console.error(err),
      );
  }
}
export interface PeriodicElement {
  1?: string;
  2?: string;
  3?: string;
  4?: string;
}
