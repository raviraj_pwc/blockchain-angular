export class testDataStructure{
  constructor(
    public role_id?: string,
    public role_name?: string,
    public participant_id?: string,
    public participant_name?: string,
    public bank_name?: string,
    public bank_branch?: string,
    public bank_ifsc?: string,
    public bank_acc_no?: string
)
  {}

}
