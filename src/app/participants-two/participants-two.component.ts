import { Component } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatSort, MatTableDataSource,MatPaginator } from "@angular/material";
import{ ViewChild} from "@angular/core";
import {TestApiService} from "../test-api.service";

@Component({
  selector: 'app-participants-two',
  templateUrl: './participants-two.component.html',
  styleUrls: ['./participants-two.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ParticipantsTwoComponent  {

  dataSource: any;
  columnsToDisplay = ['roleName', 'name', 'roleID']
  @ViewChild(MatSort) sort: MatSort;

  characters: PeriodicElement[];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private tservice: TestApiService)  {

    this.tservice.getCharacters().subscribe((data:any) => {
      console.log(data)
      this.characters = data.participants;
      console.log(this.characters)
      this.dataSource = new MatTableDataSource(this.characters);
      this.dataSource.paginator = this.paginator; //This is the paginator code
      this.dataSource.sort = this.sort;
    });
  /*Tester Code
  .subscribe(
    (data : any)  => {
        console.log(data);
        this.characters=data.participants;
        console.log('Chars: ',this.characters)
        this.dataSource = new MatTableDataSource(this.characters);

        this.dataSource.paginator = this.paginator; //This is the paginator code
        this.dataSource.sort = this.sort;

      }
    )*/
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}


export interface PeriodicElement {


      name: string
      id: string
      role: {
        id: string
        role : string
      }
}
