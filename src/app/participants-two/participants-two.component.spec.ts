import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantsTwoComponent } from './participants-two.component';

describe('ParticipantsTwoComponent', () => {
  let component: ParticipantsTwoComponent;
  let fixture: ComponentFixture<ParticipantsTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantsTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantsTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
