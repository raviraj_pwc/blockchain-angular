import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent} from "./navigation/navigation.component";
import { DefectiveAssetsComponent} from "./defective-assets/defective-assets.component";
import { ParticipantsComponent} from "./participants/participants.component";
import { ProductsComponent} from "./products/products.component";
import {FruitsComponent} from "./fruits/fruits.component";
import {InterceptortestComponent} from "./interceptortest/interceptortest.component";
import {TestAppComponent} from "./test-app/test-app.component";
import {ParticipantsTwoComponent} from "./participants-two/participants-two.component";
import {ReactiveComponent} from "./reactive/reactive.component";
import {AssetsComponent} from "./assets/assets.component";
import {GetAssetComponent} from "./get-asset/get-asset.component";
import {PutAttributeComponent} from "./put-attribute/put-attribute.component";
import {AttributeComponent} from "./attribute/attribute.component";

const routes: Routes = [
  {
    path:  '',
    //component: ReactiveComponent
    component: TestAppComponent
  },
  {
    path:  'nav',
    component:  NavigationComponent
  },
  {
    path:  'def',
    component:  DefectiveAssetsComponent
  },
  {
    path:  'par2',
    component:  ParticipantsTwoComponent
  },
  {
    path:  'int',
    component:  InterceptortestComponent
  },
  {
    path:  'fru',
    component:  FruitsComponent
  },
  {
    path:  'par',
    component:  ParticipantsComponent
  },
  {
    path:  'createParticipant',
    component:  TestAppComponent
  },
  {
    path:  'pro',
    component:  ProductsComponent
  },
  {
    path:  'createAsset',
    component:  AssetsComponent
  },
  {
    path:'put',
    component: PutAttributeComponent
  },
  {
    path: 'getAsset',
    component: GetAssetComponent
  },
  {
    path: 'attributes',
    component: AttributeComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
