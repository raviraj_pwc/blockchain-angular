import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {attributeData} from "./attributeDataStructure";

@Component({
  selector: 'app-attribute',
  templateUrl: './attribute.component.html',
  styleUrls: ['./attribute.component.scss']
})
export class AttributeComponent{
  test=new attributeData;
  participant_id: any ="";

  constructor(private http:HttpClient) { }

  onSubmit( ){

    alert("Thank you for your submission! Data: "+ JSON.stringify(this.test));
    console.log(this.test);
    console.log(JSON.stringify(this.test))

    this.http.post('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/asset',JSON.stringify(this.test))
      .subscribe(
        data=>this.participant_id=console.log(data),
        err => console.error(err),
      );

    alert();
  }
}
