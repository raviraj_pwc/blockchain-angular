export interface attributes{
  attribute_id?: string;
  attribute_name?: string;
  attribute_value?: string;
  attribute_unit?: string;
}
