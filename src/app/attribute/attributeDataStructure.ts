export class attributeData {
  constructor(
    public asset_key?: string,
    public participant_id?: string,
    public attributes?: attributeData[]
  )
  {}

}
