import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefectiveAssetsComponent } from './defective-assets.component';

describe('DefectiveAssetsComponent', () => {
  let component: DefectiveAssetsComponent;
  let fixture: ComponentFixture<DefectiveAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefectiveAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefectiveAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
