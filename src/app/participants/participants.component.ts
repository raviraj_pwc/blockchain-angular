import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatSort, MatTableDataSource,MatPaginator } from "@angular/material";
import {TestService} from "../test.service";
import{ ViewChild} from "@angular/core";

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ParticipantsComponent{

  dataSource: any;
  columnsToDisplay = ['id', 'name', 'username', 'email']
  @ViewChild(MatSort) sort: MatSort;

  characters: PeriodicElement[];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private tservice: TestService)  {

    this.tservice.getCharacters().subscribe(<PeriodicElement>(data) => {
      this.characters = data;
      //this.columnsToDisplay = ['id', 'name', 'username', 'email'];
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator; //This is the paginator code
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}


export interface PeriodicElement {
  id: number;
  name: string;
  username: string;
  email: string;
}






