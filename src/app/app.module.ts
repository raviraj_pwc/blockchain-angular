import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatTableModule, MatPaginatorModule} from '@angular/material';
import {MatDividerModule} from '@angular/material/divider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ParticipantsComponent } from './participants/participants.component';
import { ProductsComponent } from './products/products.component';
import { FruitsComponent } from './fruits/fruits.component';
import { DefectiveAssetsComponent } from './defective-assets/defective-assets.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { DataService} from "./data.service";
import { TestService} from "./test.service";
import {FormsModule} from "@angular/forms";
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MatDialogModule,MatCardModule,MatInputModule,MatButtonModule,MatDatepickerModule,MatNativeDateModule,MatSnackBarModule} from "@angular/material";
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {MatSortModule} from '@angular/material/sort';
import { InterceptortestComponent } from './interceptortest/interceptortest.component';
import { AuthService} from "./services/auth.service";
import { TestAppComponent } from './test-app/test-app.component';
import { ParticipantsTwoComponent } from './participants-two/participants-two.component';
import { TestApiService} from "./test-api.service";
import { ReactiveComponent } from './reactive/reactive.component';
import { AssetsComponent } from './assets/assets.component';
import { GetAssetComponent } from './get-asset/get-asset.component';
import { TestApi_Service} from "./get-asset/test-api.service";
import {MatExpansionModule} from '@angular/material/expansion';
import { PutAttributeComponent } from './put-attribute/put-attribute.component';
import { AttributeComponent } from './attribute/attribute.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ParticipantsComponent,
    ProductsComponent,
    FruitsComponent,
    DefectiveAssetsComponent,
    InterceptortestComponent,
    TestAppComponent,
    ParticipantsTwoComponent,
    ReactiveComponent,
    AssetsComponent,
    GetAssetComponent,
    PutAttributeComponent,
    AttributeComponent
  ],
  imports: [
    MatSortModule, MatDividerModule, MatExpansionModule,FormsModule, BrowserModule,MatTableModule,HttpClientModule,BrowserAnimationsModule,MatDialogModule,MatCardModule, MatSelectModule,
    MatInputModule,MatButtonModule,MatDatepickerModule,MatNativeDateModule, MatListModule, MatSnackBarModule, MatPaginatorModule, AppRoutingModule,
  ],
  providers: [
    DataService, TestService, AuthService, TestApiService, TestApi_Service
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
