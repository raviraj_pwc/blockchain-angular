import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }
  url = 'https://jsonplaceholder.typicode.com/users';
  getCharacters() {
    return this.http.get(`${this.url}`);
  }
}
