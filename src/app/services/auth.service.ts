import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable} from 'rxjs'
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from "@angular/common/http";
import { Headers} from "@angular/http";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private serviceUrl = 'http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/test/123'
  /*httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidXNlci1hcGkiXSwidXNlcl9uYW1lIjoiYWRtaW5AYWRtaW4uY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIiwidHJ1c3QiXSwiZXhwIjoxODUyOTU4NzQwLCJhdXRob3JpdGllcyI6WyJST0xFX1NVUEVSX0FETUlOIl0sImp0aSI6IjQxMzE1ZjE1LTA0OWMtNDBiNS1hOGFlLTM5MDU0NDE3MzZkMSIsImNsaWVudF9pZCI6InN1cnZleS1hcGktY2xpZW50In0.Xuw9yT7moY6MnfAXzYDr82bEAJ8FEFhWkUGrOoquar4'
    })
  };*/

  constructor(private http:HttpClient) {  }
  getFromAPI(): Observable<any> {
    return this.http.get<any>(this.serviceUrl);
  }
}
