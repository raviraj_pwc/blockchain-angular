import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss']
})
export class ReactiveComponent {

  constructor() {
    var observable = Observable.create((observer:any) => {
      observer.next('Hey Gusy!!')
      observer.next('WHatsup')
      observer.next('Hey Gusy!!')
      observer.next('WHatsup')
      observer.next('Hey Gusy!!')
      observer.next('WHatsup')
      observer.next('Hey Gusy!!')
      observer.next('WHatsup')
    })
    
    observable.subscribe((data:any)=>{
      console.log(data);
    })
  }


}
