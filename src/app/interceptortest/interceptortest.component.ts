import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {AuthService} from "../services/auth.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Observable} from "rxjs/internal/Observable";

@Component({
  selector: 'app-interceptortest',
  templateUrl: './interceptortest.component.html',
  styleUrls: ['./interceptortest.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])]
})
export class InterceptortestComponent {

  dataSource: any;
  //columnsToDisplay: any;
  columnsToDisplay = ['hello'];
  @ViewChild(MatSort) sort: MatSort;

  characters: PeriodicElement[];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private tservice: AuthService) {

    console.log('Hello Jyoti')
    this.tservice.getFromAPI().subscribe(
      data =>
      {this.characters = data;
      //this.columnsToDisplay = ['id', 'name', 'username', 'email'];
      this.dataSource = new MatTableDataSource(data);
      console.log(data);
      }
      );


    /*subscribe((data) => {
      this.characters = <PeriodicElement[]>data;
      console.log("What is going on");
      //this.columnsToDisplay = ['id', 'name', 'username', 'email'];
      this.dataSource = new MatTableDataSource(<PeriodicElement[]>data);
      this.dataSource.paginator = this.paginator; //This is the paginator code
      this.dataSource.sort = this.sort;
    })*/
    ;
  }
}


export interface PeriodicElement {
  hello: string;
}



