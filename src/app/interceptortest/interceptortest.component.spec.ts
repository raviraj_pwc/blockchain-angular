import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterceptortestComponent } from './interceptortest.component';

describe('InterceptortestComponent', () => {
  let component: InterceptortestComponent;
  let fixture: ComponentFixture<InterceptortestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterceptortestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterceptortestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
