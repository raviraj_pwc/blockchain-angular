import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TestApi_Service {
  constructor(private http: HttpClient) { }
  url = 'https://jsonplaceholder.typicode.com/users';
  getAsset() {
    return this.http.get('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/asset/340dc63dc93caae787d37b3170121793a9bc46333d904699a3340e1d4bdf9a83')
  }
}
