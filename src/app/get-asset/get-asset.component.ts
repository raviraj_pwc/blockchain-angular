import { Component } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatSort, MatTableDataSource,MatPaginator,MatTable } from "@angular/material";
import{ ViewChild} from "@angular/core";
import {TestApi_Service} from "./test-api.service";
import {MatAccordion} from "@angular/material";

export interface AssetStructure {
  mfcDate: string;
  id?: string;
  serialId?: string;
  batchId?: string;
  participantAttribs?: attributes3[];
  assetKey?:string;
}
export interface attributes {
  id?: string;
  value?: string;
  name?: string;
  unit?: string;
}
export interface attributes2 {
  attributes: attributes[];
  time:string;
}
export interface attributes3 {
  attribcontainers: attributes2[];
  participantId: string;
}




@Component({
  selector: 'app-get-asset',
  templateUrl: './get-asset.component.html',
  styleUrls: ['./get-asset.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})

export class GetAssetComponent{
  hardData: AssetStructure;
  ELEMENT_DATA:AssetStructure[]=[{"mfcDate":""}]; /*=[
    {
      "mfcDate": "14/08/2018",
      "id": "1012",
      "serialId": "876",
      "batchId": "14082018MU",
      "participantAttribs": [
        {
          "attribcontainers": [
            {
              "attributes": [
                {
                  "id": "Attrib1",
                  "value": "Sky",
                  "name": "Color",

                },
                {
                  "id": "Attrib2",
                  "value": "100",
                  "name": "weight",
                  "unit": "celsius"
                }
              ],
              "time": "2018-08-14 09:58:03"
            }
          ],
          "participantId": "71ec1c19190782ad837e7496e1cacf9e9ce342bc97350010d76c206ea7e0a93b25f083"
        },
        {
          "attribcontainers": [
            {
              "attributes": [
                {
                  "id": "Attrib1",
                  "value": "Pink",
                  "name": "Color",
                },
                {
                  "id": "Attrib2",
                  "value": "50",
                  "name": "temperature",
                  "unit": "celsius"
                }
              ],
              "time": "2018-08-14 10:06:25"
            }
          ],
          "participantId": "71ec1c7a88a2f7457d1b55fdfba2c00dfd4eb10f535d1ad111bc5847cea542ff947365"
        }
      ],
      "assetKey": "340dc63dc93caae787d37b3170121793a9bc46333d904699a3340e1d4bdf9a83"
      }
  ]*/
  dataSource: any[];
  displayedColumns=[/*'date',*/ 'name', 'value','unit']

      constructor(private tservice: TestApi_Service) {
      this.tservice.getAsset().subscribe((data: AssetStructure) => {
      console.log(data, "hello");
      this.hardData=data;
      this.ELEMENT_DATA[0]=this.hardData;
      console.log("hard data", this.hardData)
      console.log("hi", this.ELEMENT_DATA)
        this.dataSource=this.hardData.participantAttribs[0].attribcontainers[0].attributes;
    })

    console.log(this.ELEMENT_DATA)
  };
}
