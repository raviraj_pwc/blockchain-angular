import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAssetComponent } from './get-asset.component';

describe('GetAssetComponent', () => {
  let component: GetAssetComponent;
  let fixture: ComponentFixture<GetAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
