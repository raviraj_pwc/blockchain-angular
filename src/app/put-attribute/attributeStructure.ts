export interface attributeStructure
{
  asset_key:string;
  participant_id: string;
  attributes : [

    {
      attribute_id:string;
      attribute_name:string;
      attribute_value:string;
      attribute_unit:string;
    }
  ]
}
