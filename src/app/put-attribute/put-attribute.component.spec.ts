import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutAttributeComponent } from './put-attribute.component';

describe('PutAttributeComponent', () => {
  let component: PutAttributeComponent;
  let fixture: ComponentFixture<PutAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
