import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {assetData} from "./assetDataStructure";
import {PeriodicElement} from "../test-app/test-app.component";
import {TestApiService} from "../test-api.service";


@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss']
})
export class AssetsComponent {
  test=new assetData;
  participant_id: any ="";
  characters: PeriodicElement;
  types: PeriodicElement[]=[{}];

  constructor(private http:HttpClient, private tservice: TestApiService) {

    this.tservice.getParticipants().subscribe((data: any) => {
      console.log(data)
      this.characters=data;
      this.types[0]=this.characters;
      console.log("zero elemetn", this.types[0])
    });
  }
  onSubmit( ){

    alert("Thank you for your submission! Data: "+ JSON.stringify(this.test));
    console.log(this.test);
    console.log(JSON.stringify(this.test))

    this.http.post('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/asset',JSON.stringify(this.test))
      .subscribe(
        data=>this.participant_id=console.log(data),
        err => console.error(err),
      );

    alert();
  }
}

export interface PeriodicElement {
  1?: string;
  2?: string;
  3?: string;
  4?: string;
}
