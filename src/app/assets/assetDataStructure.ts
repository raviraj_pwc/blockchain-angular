export class assetData {
  constructor(
  public asset_id?: string,
  public asset_name?: string,
  public batch_id?: string,
  public serial_id?: string,
  public mfc_date?: string,
  public attribute_id?: string,
  public attribute_name?: string,
  public attribute_value?: string,
  public attribute_unit?: string,

)
{}

}
