import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TestApiService {
  constructor(private http: HttpClient) { }
  url = 'https://jsonplaceholder.typicode.com/users';
  getCharacters() {
    return this.http.get('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/participant/71ec1cdf35f9c189b7b053eb61a2ef243b8a317c580c3e1f11209390979231ee914ffa')
  }
  getParticipants(){
    return this.http.get('http://ec2-13-59-13-150.us-east-2.compute.amazonaws.com:8000/getpartest')

  }
}
