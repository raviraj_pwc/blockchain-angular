import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable} from "rxjs";
import { Participant} from "./Models/Participant";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private serviceUrl = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) { }

  getUser(): Observable<Participant[]>{
    return this.http.get<Participant[]>(this.serviceUrl);
  }
}
